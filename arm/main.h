#ifndef ASM_H
#define ASM_H
void enable_irq();
int32_t disable_irq();
void restore_irq(int32_t i);
